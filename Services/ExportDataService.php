<?php

namespace Services;

/**
 * ExportDataService - общий класс для герерации данных из товаров
 */
class ExportDataService
{
    const SERVICE_PATH = "Services\\";
    const REPOSITORY_PATH = "Repositories\\";

    private $service;
    private $repository;

    public function __construct(string $service, string $repository)
    {
        $classService = self::SERVICE_PATH . $service;
        $classRepository = self::REPOSITORY_PATH . $repository;
        $this->service = new $classService;
        $this->repository = new $classRepository;
    }

    /**
     * Герерация данных из товаров
     * 
     * @return string:bool
     */
    public function getFile()
    {
        return $this->service->export($this->repository->getList());
    }
}
