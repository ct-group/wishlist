<?php
// подключение служебной части пролога
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// проверка авторизации
if (!$USER->IsAuthorized()) die();

$object = new Services\ExportDataService('ExportCsvService', 'WishlistRepository');
echo $object->getFile();
