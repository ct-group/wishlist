<?php

namespace Repositories;

/**
 * WishlistRepository - класс для получения избранных товаров
 */
final class WishlistRepository
{
    /**
     * Получение избранных товаров
     * 
     * @return array
     */
    public function getList(): array
    {
        if (!\Bitrix\Main\Loader::includeModule('catalog')) die();

        global $USER;
        $userId = $USER->GetID();

        $userObject = \CUser::GetByID($userId);
        $userArray = $userObject->Fetch();
        $wishlistIds = $userArray['UF_WISHLIST'];

        $products = [];
        // формирование массива избранных товаров с требуемыми свойствами
        foreach ($wishlistIds as $id) {
            $product = \CCatalogProduct::GetByIDEx($id);
            if ($product) {
                $products[$id]['article'] = $product['PROPERTIES']['ARTICUL']['VALUE'];
                $products[$id]['name'] = $product['NAME'];
                $products[$id]['price'] = intval($product['PRICES'][1]['PRICE']) . ' ' . $product['PRICES'][1]['CURRENCY'];
            }
        }

        return $products;
    }
}
